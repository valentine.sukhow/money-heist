CREATE TABLE skills
(
    id                      BIGSERIAL NOT NULL PRIMARY KEY,
    name                    VARCHAR(100) NOT NULL unique
);

CREATE TABLE members
(
    id                      BIGSERIAL NOT NULL PRIMARY KEY,
    name                    VARCHAR(100) NOT NULL,
    sex                     VARCHAR(1) NOT NULL,
    email                   VARCHAR(100) NOT NULL unique,
    main_skill              BIGINT NOT NULL,
    status                  VARCHAR(30) NOT NULL,

    CONSTRAINT main_skill_FK  FOREIGN KEY (main_skill) REFERENCES skills (id)
);



CREATE TABLE member_skill_level
(
    id                      BIGSERIAL NOT NULL PRIMARY KEY,
    member                  BIGINT NOT NULL,
    skill                   BIGINT NOT NULL,
    level                   VARCHAR(10) NOT NULL,

    CONSTRAINT member_FK  FOREIGN KEY (member) REFERENCES members (id) ON DELETE CASCADE,
    CONSTRAINT skill_FK  FOREIGN KEY (skill) REFERENCES skills (id) ON DELETE CASCADE
);