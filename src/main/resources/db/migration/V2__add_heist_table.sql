CREATE TABLE heists
(
    id                      BIGSERIAL NOT NULL PRIMARY KEY,
    name                    VARCHAR(100) NOT NULL unique,
    location                VARCHAR(200) NOT NULL,
    start_time              TIMESTAMP WITHOUT TIME ZONE,
    end_time                TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE heist_skill
(
    id                      BIGSERIAL NOT NULL PRIMARY KEY,
    heist                   BIGINT NOT NULL,
    skill                   BIGINT NOT NULL,
    level                   VARCHAR(10) NOT NULL,
    members                 BIGINT NOT NULL,

    CONSTRAINT heist_FK  FOREIGN KEY (heist) REFERENCES heists (id) ON DELETE CASCADE,
    CONSTRAINT skill_FK  FOREIGN KEY (skill) REFERENCES skills (id) ON DELETE CASCADE
);