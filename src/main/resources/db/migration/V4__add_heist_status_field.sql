alter table heists add column if not exists status VARCHAR(30) default 'PLANNING';

CREATE TABLE members_heists
(
    member                  BIGINT NOT NULL,
    heist                   BIGINT NOT NULL,
    CONSTRAINT member_FK  FOREIGN KEY (member) REFERENCES members (id),
    CONSTRAINT heist_FK  FOREIGN KEY (heist) REFERENCES heists (id)
);