package com.agency04.moneyheist.executor;

import com.agency04.moneyheist.entity.Heist;
import com.agency04.moneyheist.entity.HeistStatus;
import com.agency04.moneyheist.repository.HeistRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@EnableScheduling
public class HeistExecutor {

    private final Logger logger = LoggerFactory.getLogger(HeistExecutor.class);

    @Autowired
    HeistRepository heistRepository;

    @Autowired
    private ScheduledExecutorService executor;

    @Transactional
    @Scheduled(fixedDelay = 60000*60*24)
    public void startHeistAutomatically() {
        Optional<Heist> nearestHeist = heistRepository
                .findFirstByStartTimeAfterAndStatusOrderByStartTimeAsc(LocalDateTime.now(), HeistStatus.READY);
        if (nearestHeist.isPresent()) {
            Heist heist = nearestHeist.get();
            if (heist.getStartTime().isBefore(LocalDateTime.now().plusDays(1))) {
                long millis = Duration.between(LocalDateTime.now(), heist.getStartTime()).toMillis();

                executor.schedule(() -> {
                    startHeist(heist);
                }, millis, TimeUnit.MILLISECONDS);

            }
        }
    }


    @Transactional
    public void startHeist(Heist heist) {
        heist.setStatus(HeistStatus.IN_PROGRESS);
        heistRepository.save(heist);
        logger.info(String.format("Heist %s started", heist.getName()));

        long millis = Duration.between(heist.getStartTime(), heist.getEndTime()).toMillis();

        executor.schedule(() -> {
            heist.setStatus(HeistStatus.FINISHED);
            heistRepository.save(heist);
            logger.info(String.format("Heist %s finished", heist.getName()));
        }, millis, TimeUnit.MILLISECONDS);

        startHeistAutomatically();
    }

}
