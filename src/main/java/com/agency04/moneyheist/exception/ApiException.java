package com.agency04.moneyheist.exception;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class ApiException extends RuntimeException {

    protected HttpStatus status;

    protected LocalDateTime timestamp = LocalDateTime.now();

    public ApiException() {
    }

    public ApiException(String message) {
        super(message);
    }

    public ApiException(HttpStatus status) {
        this.status = status;
    }

    public ApiException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }


    public HttpStatus getStatus() {
        return status;
    }


    public LocalDateTime getTimestamp() {
        return timestamp;
    }

}
