package com.agency04.moneyheist.exception;


import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandlerController {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ExceptionDto> handleValidationExceptions(MethodArgumentNotValidException ex) {
        ExceptionDto exceptionDto = new ExceptionDto();
        exceptionDto.setInternalCode("400 Bad request");
        exceptionDto.setMessage(ex.getBindingResult().getAllErrors().stream().findFirst().get().getDefaultMessage());
        return ResponseEntity.status(400)
                .body(exceptionDto);
    }

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ExceptionDto> handleBackendServiceException(ApiException e){
        ExceptionDto exceptionDto = new ExceptionDto();
        exceptionDto.setInternalCode("400 Bad request");
        exceptionDto.message = e.getMessage();
        return new ResponseEntity<>(exceptionDto, e.status);
    }


    @ExceptionHandler(InvalidFormatException.class)
    public ResponseEntity<ExceptionDto> handleBInvalidFormatException(InvalidFormatException e){
        ExceptionDto exceptionDto = new ExceptionDto();
        exceptionDto.setInternalCode("400 Bad request");
        exceptionDto.message = e.getMessage();
        return new ResponseEntity<>(exceptionDto, HttpStatus.BAD_REQUEST);
    }
}
