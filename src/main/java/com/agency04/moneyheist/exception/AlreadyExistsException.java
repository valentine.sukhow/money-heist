package com.agency04.moneyheist.exception;

import org.springframework.http.HttpStatus;

public class AlreadyExistsException extends ApiException {

    public AlreadyExistsException() {
        super(HttpStatus.BAD_REQUEST);
    }

    public AlreadyExistsException(String message) {
        super(message,HttpStatus.BAD_REQUEST);
    }
}
