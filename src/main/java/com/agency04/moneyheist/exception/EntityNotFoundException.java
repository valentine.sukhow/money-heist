package com.agency04.moneyheist.exception;

import org.springframework.http.HttpStatus;

public class EntityNotFoundException extends ApiException{

    public EntityNotFoundException() {
        super.status = HttpStatus.NOT_FOUND;
    }

    public EntityNotFoundException(String message) {
        super(message);
        super.status = HttpStatus.NOT_FOUND;
    }
}
