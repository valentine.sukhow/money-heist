package com.agency04.moneyheist.controller;

import com.agency04.moneyheist.dto.*;
import com.agency04.moneyheist.dto.request.HeistSkillsDto;
import com.agency04.moneyheist.dto.request.MembersWrapper;
import com.agency04.moneyheist.entity.HeistStatus;
import com.agency04.moneyheist.service.HeistService;
import com.agency04.moneyheist.service.MemberService;
import com.agency04.moneyheist.service.facade.HeistMemberSkillFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.*;

@RestController
@RequestMapping("/heist")
@Validated
public class HeistController {

    @Autowired
    HeistService heistService;

    @Autowired
    HeistMemberSkillFacade heistMemberSkillFacade;

    @PostMapping
    public ResponseEntity<Void> createHeist (@Valid @RequestBody HeistDto heistDto) {
        Long heistId = heistService.saveHeist(heistDto);

        URI location = URI.create("/heist/".concat(String.valueOf(heistId)));
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setLocation(location);
        return new ResponseEntity<>(responseHeaders, HttpStatus.CREATED);
    }

    @PatchMapping("/{heist_id}/skills")
    public ResponseEntity<Void> updateHeistSkills (@PathVariable("heist_id") Long heistId,
                                                   @Valid @RequestBody HeistSkillsDto heistSkillDtoList) {
        heistService.updateHeistSkill(heistId, heistSkillDtoList.getSkills());
        URI location = URI.create("/heist/".concat(String.valueOf(heistId)));
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setLocation(location);
        return new ResponseEntity<>(responseHeaders, HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{heist_id}/eligible_members")
    public ResponseEntity<HeistSkillMemberResponse> getHeistEligibleMembers (@PathVariable("heist_id") Long heistId) {
        HeistSkillMemberResponse eligibleMembersAndHeistSkills = heistMemberSkillFacade.getEligibleMembersAndHeistSkills(heistId);
        return new ResponseEntity<>(eligibleMembersAndHeistSkills, HttpStatus.OK);
    }


    @PutMapping("/{heist_id}/members")
    public ResponseEntity<HeistSkillMemberResponse> confirmHeistMembers (@PathVariable("heist_id") Long heistId,
                                                                         @RequestBody MembersWrapper membersWrapper) {

        heistService.confirmHeistMembers(heistId, membersWrapper.members);
        URI location = URI.create("/heist/".concat(String.valueOf(heistId)));
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setLocation(location);
        return new ResponseEntity<>(responseHeaders, HttpStatus.NO_CONTENT);
    }

    @PutMapping("/{heist_id}/start")
    public ResponseEntity<HeistSkillMemberResponse> startHeist (@PathVariable("heist_id") Long heistId) {
        heistService.startHeist(heistId);
        URI location = URI.create("/heist/".concat(String.valueOf(heistId)).concat("/status"));
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setLocation(location);
        return new ResponseEntity<>(responseHeaders, HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{heist_id}")
    public ResponseEntity<HeistDto> getHeist (@PathVariable("heist_id") Long heistId) {
        HeistDto heist = heistService.getHeistDto(heistId);
        return new ResponseEntity<>(heist, HttpStatus.OK);
    }

    @GetMapping("/{heist_id}/members")
    public ResponseEntity<Set<MemberDto>> getHeistMembers (@PathVariable("heist_id") Long heistId) {
        Set<MemberDto> members = heistMemberSkillFacade.getHeistMembers(heistId);
        return new ResponseEntity<>(members, HttpStatus.OK);
    }

    @GetMapping("/{heist_id}/skills")
    public ResponseEntity<List<HeistSkillDto>> getHeistSkills (@PathVariable("heist_id") Long heistId) {
        List<HeistSkillDto> heistSkills = heistMemberSkillFacade.getHeistSkills(heistId);
        return new ResponseEntity<>(heistSkills, HttpStatus.OK);
    }


    @GetMapping("/{heist_id}/status")
    public ResponseEntity<Map<String, HeistStatus>> getHeistStatua (@PathVariable("heist_id") Long heistId) {
        HeistStatus heistStatus = heistService.getHeistStatus(heistId);
        Map<String, HeistStatus> statusMap = new HashMap<>(){{
            put("status", heistStatus);
        }};
        return new ResponseEntity<>(statusMap, HttpStatus.OK);
    }
}
