package com.agency04.moneyheist.controller;

import com.agency04.moneyheist.dto.HeistSkillMemberResponse;
import com.agency04.moneyheist.dto.MemberDto;
import com.agency04.moneyheist.dto.SkillsDto;
import com.agency04.moneyheist.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/member")
@Validated
public class MemberController {

    @Autowired
    MemberService memberService;

    @PostMapping
    public ResponseEntity<MemberDto> createMember (@Valid @RequestBody MemberDto memberDto) {
        MemberDto member = memberService.createMember(memberDto);
        return new ResponseEntity<>(member, HttpStatus.CREATED);
    }

    @PutMapping("/{member_id}/skills")
    public ResponseEntity<Void> updateMemberSkills (@PathVariable("member_id") Long memberId,
                                                         @Valid @RequestBody SkillsDto skillsDto) {
        memberService.updateMemberSkills(memberId, skillsDto);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{member_id}/skills/{skill_name}")
    public ResponseEntity<Void> deleteMemberSkill (@PathVariable("member_id") Long memberId,
                                                   @PathVariable("skill_name") String skillName) {
        memberService.deleteMemberSkills(memberId, skillName);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{member_id}")
    public ResponseEntity<MemberDto> getMember (@PathVariable("member_id") Long memberId) {
        MemberDto member = memberService.getMember(memberId);
        return new ResponseEntity<>(member, HttpStatus.OK);
    }


    @GetMapping("/{member_id}/skills")
    public ResponseEntity<SkillsDto> getMemberSkills (@PathVariable("member_id") Long memberId) {
        SkillsDto memberSkills = memberService.getMemberSkills(memberId);
        return new ResponseEntity<>(memberSkills, HttpStatus.OK);
    }




}
