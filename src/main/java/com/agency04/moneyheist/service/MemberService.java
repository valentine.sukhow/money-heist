package com.agency04.moneyheist.service;

import com.agency04.moneyheist.dto.HeistSkillMemberResponse;
import com.agency04.moneyheist.dto.MemberDto;
import com.agency04.moneyheist.dto.SkillsDto;
import com.agency04.moneyheist.entity.Member;
import com.agency04.moneyheist.entity.Skill;

import java.util.List;
import java.util.Set;

public interface MemberService {

    MemberDto createMember(MemberDto memberDto);

    void updateMemberSkills(Long memberId, SkillsDto skillsDto);

    void deleteMemberSkills(Long memberId, String skillName);

    MemberDto getMember(Long memberId);

    SkillsDto getMemberSkills(Long memberId);

    HeistSkillMemberResponse getEligibleMembersAndHeistSkills(Long heistId);

    List<MemberDto> hetHeistMembers(Long heistId);

    Set<Member> getMembersWithExactSkillAndNecessaryLevel(Skill skill, String level);

}
