package com.agency04.moneyheist.service;

import com.agency04.moneyheist.dto.HeistDto;
import com.agency04.moneyheist.dto.HeistSkillDto;
import com.agency04.moneyheist.entity.Heist;
import com.agency04.moneyheist.entity.HeistStatus;

import java.util.List;

public interface HeistService {

    Long saveHeist(HeistDto heistDto);

    void updateHeistSkill(Long heistId, List<HeistSkillDto> heistSkillDtoList);

    void confirmHeistMembers(Long heistId, List<String> members);

    void startHeist(Long heistId);

    HeistDto getHeistDto(Long heistId);

    Heist getHeist(Long heistId);

    HeistStatus getHeistStatus(Long heistId);
}
