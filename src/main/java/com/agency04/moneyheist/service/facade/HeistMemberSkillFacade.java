package com.agency04.moneyheist.service.facade;

import com.agency04.moneyheist.dto.*;
import com.agency04.moneyheist.entity.Heist;
import com.agency04.moneyheist.entity.HeistSkill;
import com.agency04.moneyheist.entity.Member;
import com.agency04.moneyheist.entity.Skill;
import com.agency04.moneyheist.exception.EntityNotFoundException;
import com.agency04.moneyheist.mapper.HeistSkillMapper;
import com.agency04.moneyheist.mapper.MemberMapper;
import com.agency04.moneyheist.mapper.SkillMapper;
import com.agency04.moneyheist.repository.HeistSkillRepository;
import com.agency04.moneyheist.service.HeistService;
import com.agency04.moneyheist.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class HeistMemberSkillFacade {

    @Autowired
    private HeistService heistService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private HeistSkillRepository heistSkillRepository;

    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private HeistSkillMapper heistSkillMapper;


    public HeistSkillMemberResponse getEligibleMembersAndHeistSkills(Long heistId) {
        HeistSkillMemberResponse heistSkillMemberResponse = new HeistSkillMemberResponse();
        List<SkillDtoWithMembers> skills = new ArrayList<>();

        Set<Member> members = new HashSet<>();

        Heist heist = heistService.getHeist(heistId);
        List<HeistSkill> heistSkills = heistSkillRepository.findSkillByHeist(heist);

        for (HeistSkill heistSkill : heistSkills) {

            SkillDtoWithMembers skillDtoWithMembers = new SkillDtoWithMembers(heistSkill.getSkill().getName(),
                    heistSkill.getLevel(), Math.toIntExact(heistSkill.getMembers()));

            skills.add(skillDtoWithMembers);

            Set<Member> membersWithThisSkill = memberService
                    .getMembersWithExactSkillAndNecessaryLevel(heistSkill.getSkill(), heistSkill.getLevel());

            members.addAll(membersWithThisSkill);
        }

        heistSkillMemberResponse.skills = skills;
        heistSkillMemberResponse.members = memberMapper.convertMembers(members);
        return heistSkillMemberResponse;
    }

    public Set<MemberDto> getHeistMembers(Long heistId) {
        Heist heist = heistService.getHeist(heistId);
        Set<Member> members = heist.getMembers();
        return memberMapper.convertMembers(members);
    }

    public List<HeistSkillDto> getHeistSkills(Long heistId) {
        Heist heist = heistService.getHeist(heistId);
        return heistSkillMapper.convertSetDto(heist.getSkills());
    }

}
