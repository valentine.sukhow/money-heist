package com.agency04.moneyheist.service;

import com.agency04.moneyheist.entity.Skill;

public interface SkillService {

    Skill saveOrReturnSavedSkill(String name);

    Skill getSkillByNameOrThrowException(String name);
}
