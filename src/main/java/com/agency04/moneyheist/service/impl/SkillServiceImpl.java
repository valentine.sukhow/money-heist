package com.agency04.moneyheist.service.impl;

import com.agency04.moneyheist.entity.Skill;
import com.agency04.moneyheist.exception.EntityNotFoundException;
import com.agency04.moneyheist.repository.SkillRepository;
import com.agency04.moneyheist.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SkillServiceImpl implements SkillService {

    @Autowired
    SkillRepository skillRepository;

    @Override
    public Skill saveOrReturnSavedSkill(String name) {
        Optional<Skill> skill = skillRepository.findByName(name);
        return skill.orElseGet(() -> skillRepository.save(new Skill(name)));
    }

    @Override
    public Skill getSkillByNameOrThrowException(String name) {

        Optional<Skill> optionalSkill = skillRepository.findByName(name);
        if (optionalSkill.isEmpty()) {
            throw new EntityNotFoundException(String.format("Skill with name %s does not exist", name));
        }
        return optionalSkill.get();
    }


}
