package com.agency04.moneyheist.service.impl;

import com.agency04.moneyheist.dto.*;
import com.agency04.moneyheist.entity.*;
import com.agency04.moneyheist.exception.AlreadyExistsException;
import com.agency04.moneyheist.exception.ApiException;
import com.agency04.moneyheist.exception.EntityNotFoundException;
import com.agency04.moneyheist.mapper.MemberMapper;
import com.agency04.moneyheist.mapper.SkillMapper;
import com.agency04.moneyheist.repository.HeistRepository;
import com.agency04.moneyheist.repository.HeistSkillRepository;
import com.agency04.moneyheist.repository.MemberRepository;
import com.agency04.moneyheist.repository.MemberSkillLevelRepository;
import com.agency04.moneyheist.service.MemberService;
import com.agency04.moneyheist.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class MemberServiceImpl implements MemberService {

    @Autowired
    MemberMapper memberMapper;

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    SkillService skillService;

    @Autowired
    MemberSkillLevelRepository memberSkillLevelRepository;

    @Autowired
    HeistRepository heistRepository;

    @Autowired
    SkillMapper skillMapper;

    @Autowired
    HeistSkillRepository heistSkillRepository;

    @Override
    public MemberDto createMember(MemberDto memberDto) {
        checkMemberForUnique(memberDto);
        Member member = memberMapper.convert(memberDto);
        if (memberDto.getMainSkill() != null) {
            if (memberDto.getSkills().stream().map(SkillDto::getName).collect(Collectors.toSet()).contains(memberDto.getMainSkill())) {
                member.setMainSkill(skillService.saveOrReturnSavedSkill(memberDto.getMainSkill().toLowerCase()));
            } else {
                throw new ApiException("Main skill must be part of skills array", HttpStatus.BAD_REQUEST);
            }
        }

        Member savedMember = memberRepository.save(member);

        memberDto.skills.forEach(skillDto -> {
            Skill skill = skillService.saveOrReturnSavedSkill(skillDto.getName().toLowerCase());
            memberSkillLevelRepository.save(new MemberSkillLevel(savedMember, skill, skillDto.getLevel()));
        });


        return memberDto;
    }

    private void checkMemberForUnique(MemberDto memberDto) {
        if (memberRepository.findByEmail(memberDto.getEmail()).isPresent()) {
            throw new AlreadyExistsException(String.format("Member with email %s already registered", memberDto.getEmail()));
        }
        if (memberRepository.findByName(memberDto.getName()).isPresent()) {
            throw new AlreadyExistsException(String.format("Member with name %s already registered", memberDto.getName()));
        }
    }

    @Override
    public void updateMemberSkills(Long memberId, SkillsDto skillsDto) {
        Member member = getMemberOrThrowNotExists(memberId);

        if (skillsDto.getMainSkill() == null && skillsDto.getSkills() == null) {
            throw new ApiException("Not enough data to update skill", HttpStatus.BAD_REQUEST);
        }

        if (skillsDto.getMainSkill() != null) {
            String mainSkill = skillsDto.getMainSkill();

            Set<String> memberSkillNames = member.getSkills().stream()
                    .map(skill -> skill.getSkill().getName()).collect(Collectors.toSet());

            Set<String> newSkillsFromDto = new HashSet<>();

            if (skillsDto.getSkills() != null) {
                newSkillsFromDto = skillsDto.getSkills().stream().map(skill -> skill.getName()).collect(Collectors.toSet());
            }

            if (!memberSkillNames.contains(mainSkill) && !newSkillsFromDto.contains(mainSkill)) {
                throw new ApiException("Main skill is not part of the member’s previous or updated", HttpStatus.BAD_REQUEST);
            } else {
                Skill skill = skillService.saveOrReturnSavedSkill(skillsDto.getMainSkill());
                memberRepository.updateMemberSkillLevel(memberId, skill.getId());
            }
        }

        if (skillsDto.getSkills() != null) {
            for (SkillDto skill : skillsDto.getSkills()) {
                Skill savedSkill = skillService.saveOrReturnSavedSkill(skill.getName());

                Optional<MemberSkillLevel> memberSkillLevel = memberSkillLevelRepository.findByMemberAndSkill(member, savedSkill);
                if (memberSkillLevel.isPresent()) {
                    memberSkillLevelRepository.updateMemberSkillLevel(memberId, savedSkill.getId(), skill.getLevel());
                } else {
                    memberSkillLevelRepository.save(new MemberSkillLevel(member, savedSkill, skill.getLevel()));
                }
            }
        }
    }

    @Override
    public void deleteMemberSkills(Long memberId, String skillName) {
        Member member = getMemberOrThrowNotExists(memberId);
        Optional<MemberSkillLevel> memberSkillLevel = memberSkillLevelRepository
                .findByMemberAndSkill(member, skillService.getSkillByNameOrThrowException(skillName));

        if (memberSkillLevel.isEmpty()) {
            throw new EntityNotFoundException("Member’s skill does not exist.");
        } else {
            memberSkillLevelRepository.delete(memberSkillLevel.get());
        }
    }

    @Override
    public MemberDto getMember(Long memberId) {
        Member member = getMemberOrThrowNotExists(memberId);
        return memberMapper.convert(member);
    }

    @Override
    public SkillsDto getMemberSkills(Long memberId) {
        Member member = getMemberOrThrowNotExists(memberId);
        SkillsDto skillsDto = new SkillsDto();
        Set<SkillDto> skills = member.getSkills().stream()
                .map(skill -> new SkillDto(skill.getSkill().getName(), skill.getLevel())).collect(Collectors.toSet());
        skillsDto.setSkills(skills);
        skillsDto.setMainSkill(member.getMainSkill().getName());
        return skillsDto;
    }

    @Override
    public HeistSkillMemberResponse getEligibleMembersAndHeistSkills(Long heistId) {
        HeistSkillMemberResponse heistSkillMemberResponse = new HeistSkillMemberResponse();
        List<SkillDtoWithMembers> skills = new ArrayList<>();

        Set<Member> members = new HashSet<>();

        Optional<Heist> optionalHeist = heistRepository.findById(heistId);
        if (optionalHeist.isEmpty()) {
            throw new EntityNotFoundException(String.format("Heist with id %s not found", heistId));
        }
        Heist heist = optionalHeist.get();
        List<HeistSkill> heistSkills = heistSkillRepository.findSkillByHeist(heist);

        for (HeistSkill heistSkill : heistSkills) {

            SkillDtoWithMembers skillDtoWithMembers = new SkillDtoWithMembers(heistSkill.getSkill().getName(),
                    heistSkill.getLevel(), Math.toIntExact(heistSkill.getMembers()));

            skills.add(skillDtoWithMembers);

            Set<Member> membersWithThisSkill = memberRepository
                    .findBySkillAndLevelContains(heistSkill.getSkill(), heistSkill.getLevel());

            members.addAll(membersWithThisSkill);
        }

        heistSkillMemberResponse.skills = skills;
        heistSkillMemberResponse.members = memberMapper.convertMembers(members);
        return heistSkillMemberResponse;
    }

    @Override
    public List<MemberDto> hetHeistMembers(Long heistId) {
        return null;
    }

    @Override
    public Set<Member> getMembersWithExactSkillAndNecessaryLevel(Skill skill, String level) {
        return memberRepository.findBySkillAndLevelContains(skill, level);
    }

    private boolean isSkillEquals(HeistSkill heistSkill, MemberSkillLevel memberSkill) {
        return heistSkill.getSkill().getName().equals(memberSkill.getSkill().getName())
                && memberSkill.getLevel().length() >= heistSkill.getLevel().length();
    }

    private Member getMemberOrThrowNotExists(Long memberId) {
        Optional<Member> optionalMember = memberRepository.findById(memberId);
        if (optionalMember.isEmpty()) {
            throw new EntityNotFoundException(String.format("Member with id %s does not exist", memberId));
        }
        return optionalMember.get();
    }
}