package com.agency04.moneyheist.service.impl;

import com.agency04.moneyheist.dto.HeistDto;
import com.agency04.moneyheist.dto.HeistSkillDto;
import com.agency04.moneyheist.entity.*;
import com.agency04.moneyheist.exception.AlreadyExistsException;
import com.agency04.moneyheist.exception.ApiException;
import com.agency04.moneyheist.exception.EntityNotFoundException;
import com.agency04.moneyheist.mapper.HeistMapper;
import com.agency04.moneyheist.mapper.HeistSkillMapper;
import com.agency04.moneyheist.repository.HeistRepository;
import com.agency04.moneyheist.repository.HeistSkillRepository;
import com.agency04.moneyheist.repository.MemberRepository;
import com.agency04.moneyheist.service.HeistService;
import com.agency04.moneyheist.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class HeistServiceImpl implements HeistService {

    @Autowired
    HeistRepository heistRepository;

    @Autowired
    HeistSkillRepository heistSkillRepository;

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    HeistMapper heistMapper;

    @Autowired
    SkillService skillService;

    @Autowired
    HeistSkillMapper heistSkillMapper;

    @Override
    public Long saveHeist(HeistDto heistDto) {
        if (heistRepository.findByName(heistDto.getName()).isPresent()) {
            throw new AlreadyExistsException(String.format("Heist with name %s already registered", heistDto.getName()));
        }
        if (heistDto.getStartTime().isAfter(heistDto.getEndTime())) {
            throw new ApiException("StartTime couldn't be after endTime", HttpStatus.BAD_REQUEST);
        }
        checkForSkillsWithSameNameAndLevel(heistDto.getSkills());

        Heist heist = heistMapper.convert(heistDto);
        Heist savedHeist = heistRepository.save(heist);

        for (HeistSkillDto skill : heistDto.getSkills()) {
            HeistSkill heistSkill = heistSkillMapper.convert(skill);
            heistSkill.setHeist(savedHeist);
            heistSkillRepository.save(heistSkill);
        }

        return savedHeist.getId();
    }

    @Override
    public void updateHeistSkill(Long heistId, List<HeistSkillDto> heistSkillDtoList) {
        checkForSkillsWithSameNameAndLevel(heistSkillDtoList);
        Heist heist = getHeist(heistId);
        if (heist.getStartTime().isBefore(LocalDateTime.now())) {
            throw new ApiException("Heist has already started, you couldn't update heist's skills", HttpStatus.METHOD_NOT_ALLOWED);
        }

        for (HeistSkillDto heistSkillDto : heistSkillDtoList) {

            Skill skill = skillService.getSkillByNameOrThrowException(heistSkillDto.getName());

            Optional<HeistSkill> heistSkills = heistSkillRepository
                    .findByHeistAndSkillAndLevel(heist, skill, heistSkillDto.getLevel());
            if (heistSkills.isPresent()) {
                heistSkillRepository.updateMembersCount(heistSkillDto.getMembers(), heistSkills.get().id);
            } else {
                if (heistSkillRepository.findByHeistAndSkill(heist, skill).stream().findFirst().isPresent()) {
                    HeistSkill heistSkill = heistSkillRepository.findByHeistAndSkill(heist, skill)
                            .stream().findFirst().get();

                    heistSkill.setMembers(heistSkillDto.getMembers());
                    heistSkill.setLevel(heistSkillDto.getLevel());
                    heistSkillRepository.save(heistSkill);
                } else {
                    HeistSkill heistSkill = heistSkillMapper.convert(heistSkillDto);
                    heistSkill.setHeist(heist);
                    heistSkillRepository.save(heistSkill);
                }
            }
        }
    }


    @Override
    public void confirmHeistMembers(Long heistId, List<String> members) {
        Heist heist = getHeist(heistId);
        if (!heist.getStatus().equals(HeistStatus.PLANNING)) {
            throw new ApiException("Heist status is not PLANNING", HttpStatus.METHOD_NOT_ALLOWED);
        }

        Set<Member> memberSet = new HashSet<>();

        for (String memberName : members) {
            memberSet.add(getAndValidateMemberByName(heist, memberName));
        }

        heist.setStatus(HeistStatus.READY);
        heist.setMembers(memberSet);
    }

    private Member getAndValidateMemberByName(Heist heist, String name) {
        Optional<Member> optionalMember = memberRepository.findByName(name);
        if (optionalMember.isEmpty()) {
            throw new ApiException(String.format("Member with name %s does not exist", name), HttpStatus.BAD_REQUEST);
        }
        Member member = optionalMember.get();
        if (!member.getStatus().equals(Status.AVAILABLE)) {
            if (!member.getStatus().equals(Status.RETIRED)) {
                throw new ApiException(String.format("Member with name %s has %s status, but should be Available or Retired",
                        name, member.getStatus()), HttpStatus.BAD_REQUEST);
            }
        }

        Set<Skill> memberSkills = member.getSkills().stream().map(MemberSkillLevel::getSkill).collect(Collectors.toSet());
        Set<Skill> heistSkill = heist.getSkills().stream().map(HeistSkill::getSkill).collect(Collectors.toSet());

        heistSkill.retainAll(memberSkills);

        if (heistSkill.size() == 0) {
            throw new ApiException(
                    String.format("%s skills doesn't match at least one of the required skills of the heist", name),
                    HttpStatus.BAD_REQUEST);
        }

        for (Heist memberHeist : member.getHeists()) {
            LocalDateTime start = heist.getStartTime();
            LocalDateTime end = heist.getEndTime();
            if (isDateBetween(start, memberHeist.getStartTime(), memberHeist.getEndTime()) ||
                    isDateBetween(end, memberHeist.getStartTime(), memberHeist.getEndTime()))          {
                throw new ApiException(
                        String.format("%s is already confirmed member of another heist happening at the same time", name),
                        HttpStatus.BAD_REQUEST);
            }
        }
        return member;
    }

    private boolean isDateBetween(LocalDateTime origin, LocalDateTime start, LocalDateTime end) {
        return !origin.isBefore(start) && !origin.isAfter(end);
    }

    @Override
    public void startHeist(Long heistId) {
        Heist heist = getHeist(heistId);
        if (!heist.getStatus().equals(HeistStatus.READY)) {
            throw new ApiException("Heist status is not READY", HttpStatus.METHOD_NOT_ALLOWED);
        }
        heist.setStatus(HeistStatus.IN_PROGRESS);
    }

    @Override
    public HeistDto getHeistDto(Long heistId) {
        Heist heist = getHeist(heistId);
        return heistMapper.convert(heist);
    }

    @Override
    public Heist getHeist(Long heistId) {
        Optional<Heist> optionalHeist = heistRepository.findById(heistId);
        if (optionalHeist.isEmpty()) {
            throw new EntityNotFoundException(String.format("Heist with id %s not found", heistId));
        }
        return optionalHeist.get();
    }

    @Override
    public HeistStatus getHeistStatus(Long heistId) {
        return getHeist(heistId).getStatus();
    }

    private void checkForSkillsWithSameNameAndLevel(List<HeistSkillDto> heistSkillDtoList) {
        Set<HeistSkillDto> heistSkillDtoSet = new HashSet<>(){{
            addAll(heistSkillDtoList);
        }};

        if (heistSkillDtoList.size() != heistSkillDtoSet.size()) {
            throw new ApiException("Multiple skills with the same name and level were provided", HttpStatus.BAD_REQUEST);
        }
    }
}
