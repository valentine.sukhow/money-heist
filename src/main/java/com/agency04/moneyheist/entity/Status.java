package com.agency04.moneyheist.entity;

public enum Status {

    AVAILABLE,
    EXPIRED,
    INCARCERATED,
    RETIRED

}
