package com.agency04.moneyheist.entity;

public enum HeistStatus {

    FINISHED,
    IN_PROGRESS,
    PLANNING,
    READY

}
