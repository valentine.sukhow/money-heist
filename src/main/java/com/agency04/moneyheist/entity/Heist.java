package com.agency04.moneyheist.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "heists")
public class Heist extends BaseEntity {

    private String name;

    private String location;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    @LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(mappedBy="heist")
    private Set<HeistSkill> skills;

    @Enumerated(EnumType.STRING)
    private HeistStatus status;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(
        name = "members_heists",
            joinColumns = @JoinColumn(name = "heist", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "member", referencedColumnName = "id")
    )
    private Set<Member> members;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public HeistStatus getStatus() {
        return status;
    }

    public void setStatus(HeistStatus status) {
        this.status = status;
    }

    public Set<HeistSkill> getSkills() {
        return skills;
    }

    public void setSkills(Set<HeistSkill> skills) {
        this.skills = skills;
    }

    public Set<Member> getMembers() {
        return members;
    }

    public void setMembers(Set<Member> members) {
        this.members = members;
    }
}
