package com.agency04.moneyheist.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "members")
public class Member extends BaseEntity {

    private String name;

    @Enumerated(EnumType.STRING)
    private Sex sex;

    private String email;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="main_skill")
    private Skill mainSkill;

    @Enumerated(EnumType.STRING)
    private Status status;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy="member")
    private Set<MemberSkillLevel> skills;

    @ManyToMany(mappedBy = "members")
    private Set<Heist> heists;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Skill getMainSkill() {
        return mainSkill;
    }

    public void setMainSkill(Skill mainSkill) {
        this.mainSkill = mainSkill;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<MemberSkillLevel> getSkills() {
        return skills;
    }

    public void setSkills(Set<MemberSkillLevel> skills) {
        this.skills = skills;
    }

    public Set<Heist> getHeists() {
        return heists;
    }

    public void setHeists(Set<Heist> heists) {
        this.heists = heists;
    }
}
