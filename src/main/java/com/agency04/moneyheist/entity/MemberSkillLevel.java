package com.agency04.moneyheist.entity;

import javax.persistence.*;

@Entity
@Table(name = "member_skill_level")
public class MemberSkillLevel extends BaseEntity {

    @ManyToOne
    @JoinColumn(name="member", nullable=false)
    private Member member;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="skill", nullable=false)
    private Skill skill;

    private String level;

    public MemberSkillLevel(Member member, Skill skill, String level) {
        this.member = member;
        this.skill = skill;
        this.level = level;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public MemberSkillLevel() {
    }
}
