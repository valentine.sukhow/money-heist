package com.agency04.moneyheist.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "skills")
public class Skill extends BaseEntity {

    private String name;

    @LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(mappedBy="skill")
    private List<MemberSkillLevel> skillLevels;

    public Skill() {
    }

    public Skill(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MemberSkillLevel> getSkillLevels() {
        return skillLevels;
    }

    public void setSkillLevels(List<MemberSkillLevel> skillLevels) {
        this.skillLevels = skillLevels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Skill skill = (Skill) o;

        return new EqualsBuilder().appendSuper(super.equals(o)).append(name, skill.name).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).appendSuper(super.hashCode()).append(name).toHashCode();
    }
}
