package com.agency04.moneyheist.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "heist_skill")
public class HeistSkill extends BaseEntity {

    @ManyToOne
    @JoinColumn(name="heist", nullable=false)
    private Heist heist;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="skill")
    private Skill skill;

    private String level;

    private Long members;

    public Heist getHeist() {
        return heist;
    }

    public void setHeist(Heist heist) {
        this.heist = heist;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Long getMembers() {
        return members;
    }

    public void setMembers(Long members) {
        this.members = members;
    }


}
