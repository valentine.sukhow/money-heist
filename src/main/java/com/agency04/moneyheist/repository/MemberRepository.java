package com.agency04.moneyheist.repository;

import com.agency04.moneyheist.entity.Member;
import com.agency04.moneyheist.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.Set;

public interface MemberRepository extends JpaRepository<Member, Long> {

    Optional<Member> findByEmail(String email);

    Optional<Member> findByName(String name);

    @Modifying
    @Query(value = "UPDATE members SET main_skill = :skill where id = :id", nativeQuery = true)
    void updateMemberSkillLevel(@Param("id") Long id, @Param("skill") Long skill);

    @Query(value = "SELECT m.* FROM members m join member_skill_level msl on msl.member = m.id " +
            "where m.status in('AVAILABLE', 'RETIRED') and msl.skill = :skill and msl.level like :level%", nativeQuery = true)
    Set<Member> findBySkillAndLevelContains(Skill skill, String level);
}
