package com.agency04.moneyheist.repository;

import com.agency04.moneyheist.entity.Heist;
import com.agency04.moneyheist.entity.HeistSkill;
import com.agency04.moneyheist.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface HeistSkillRepository extends JpaRepository<HeistSkill, Long> {

    List<HeistSkill> findByHeistAndSkill(Heist heist, Skill skill);

    Optional<HeistSkill> findByHeistAndSkillAndLevel(Heist heist, Skill skill, String level);

    @Modifying
    @Query(value = "UPDATE heist_skill SET members = :membersCount where id = :id", nativeQuery = true)
    void updateMembersCount(@Param("membersCount") Long membersCount, @Param("id") Long id);

    List<HeistSkill> findSkillByHeist(Heist heist);

}
