package com.agency04.moneyheist.repository;

import com.agency04.moneyheist.entity.Member;
import com.agency04.moneyheist.entity.MemberSkillLevel;
import com.agency04.moneyheist.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MemberSkillLevelRepository extends JpaRepository<MemberSkillLevel, Long> {

    Optional<MemberSkillLevel> findByMemberAndSkill(Member member, Skill skill);

    @Modifying
    @Query(value = "UPDATE member_skill_level SET level = :level where member = :member and skill = :skill", nativeQuery = true)
    void updateMemberSkillLevel(@Param("member") Long member, @Param("skill") Long skill, @Param("level") String level);



}
