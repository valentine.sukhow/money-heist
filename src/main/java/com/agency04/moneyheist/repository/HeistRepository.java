package com.agency04.moneyheist.repository;

import com.agency04.moneyheist.entity.Heist;
import com.agency04.moneyheist.entity.HeistStatus;
import com.agency04.moneyheist.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Optional;

public interface HeistRepository extends JpaRepository<Heist, Long> {

    Optional<Heist> findByName(String name);

    Optional<Heist> findFirstByStartTimeAfterAndStatusOrderByStartTimeAsc(LocalDateTime currentTime, HeistStatus heistStatus);
}
