package com.agency04.moneyheist.mapper;

import com.agency04.moneyheist.dto.HeistSkillDto;
import com.agency04.moneyheist.entity.HeistSkill;
import com.agency04.moneyheist.entity.Skill;
import com.agency04.moneyheist.exception.EntityNotFoundException;
import com.agency04.moneyheist.repository.SkillRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class HeistSkillMapper {

    @Autowired
    SkillRepository skillRepository;

    @Mappings({
            @Mapping(target = "skill", source = "name", qualifiedByName = "convertNameToSkill"),
    })
    public abstract HeistSkill convert(HeistSkillDto heistSkillDto);


    @Mappings({
            @Mapping(target = "name", source = "skill", qualifiedByName = "convertSkillToName"),
    })
    public abstract HeistSkillDto convert(HeistSkill heistSkill);

    public abstract List<HeistSkillDto> convertSetDto(Set<HeistSkill> heistSkills);


    @Named("convertNameToSkill")
    Skill convertNameToSkill(String name) {
        if (skillRepository.findByName(name).isPresent()) {
            return skillRepository.findByName(name).get();
        } else {
            throw new EntityNotFoundException(String.format("Skill with name %s does not exist", name));
        }
    }

    @Named("convertSkillToName")
    String convertSkillToName(Skill skill) {
        return skill.getName();
    }


}
