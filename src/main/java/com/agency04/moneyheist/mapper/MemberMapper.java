package com.agency04.moneyheist.mapper;

import com.agency04.moneyheist.dto.MemberDto;
import com.agency04.moneyheist.dto.MemberSimpleDto;
import com.agency04.moneyheist.dto.SkillDto;
import com.agency04.moneyheist.entity.Member;
import com.agency04.moneyheist.entity.MemberSkillLevel;
import com.agency04.moneyheist.entity.Skill;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MemberMapper {

    @Autowired
    SkillMapper skillMapper;

    @Mappings({
            @Mapping(ignore = true, target = "mainSkill")
    })
    public abstract Member convert(MemberDto memberDto);

    @Mappings({
            @Mapping(source = "mainSkill", target = "mainSkill", qualifiedByName = "mapSkillToName"),
            @Mapping(source = "skills", target = "skills", qualifiedByName = "mapSkills")
    })
    public abstract MemberDto convert(Member member);

    public abstract Set<MemberDto> convertMembers(Set<Member> members);

    @Named("mapSkillToName")
    String mapSkillToName(Skill skill) {
        return skill.getName();
    }

    @Named("mapSkills")
    Set<SkillDto> mapSkills(Set<MemberSkillLevel> skills) {
        Set<SkillDto> skillDtos = new HashSet<>();
        skills.forEach(skill -> {
            SkillDto skillDto = skillMapper.convert(skill.getSkill());
            skillDto.setLevel(skill.getLevel());
            skillDtos.add(skillDto);
        });
        return skillDtos;
    }
}
