package com.agency04.moneyheist.mapper;

import com.agency04.moneyheist.dto.HeistDto;
import com.agency04.moneyheist.dto.HeistSkillDto;
import com.agency04.moneyheist.dto.MemberDto;
import com.agency04.moneyheist.entity.Heist;
import com.agency04.moneyheist.entity.HeistSkill;
import com.agency04.moneyheist.entity.Member;
import com.agency04.moneyheist.entity.Skill;
import com.agency04.moneyheist.exception.EntityNotFoundException;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class HeistMapper {

    @Autowired
    HeistSkillMapper heistSkillMapper;

    public abstract Heist convert(HeistDto heistDto);

    @Mappings({
            @Mapping(target = "skills", source = "skills", qualifiedByName = "convertHeistSkill"),
    })
    public abstract HeistDto convert(Heist heist);

    @Named("convertHeistSkill")
    List<HeistSkillDto> convertHeistSkill(Set<HeistSkill> skills) {
        return heistSkillMapper.convertSetDto(skills);
    }

}
