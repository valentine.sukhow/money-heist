package com.agency04.moneyheist.mapper;

import com.agency04.moneyheist.dto.SkillDto;
import com.agency04.moneyheist.entity.Skill;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface SkillMapper {

    Skill convert(SkillDto skillDto);

    SkillDto convert(Skill skill);

    Set<Skill> convertList(Set<SkillDto> skillDtos);

    Set<SkillDto> convertListToDto(Set<Skill> skills);

}
