package com.agency04.moneyheist.dto;

import java.util.List;
import java.util.Set;

public class HeistSkillMemberResponse {

    public List<SkillDtoWithMembers> skills;

    public Set<MemberDto> members;

}
