package com.agency04.moneyheist.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class SkillDto {

    @NotBlank
    private String name;

    @Size(min = 1, max = 10)
    private String level = "*";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public SkillDto(String name, String level) {
        this.name = name;
        this.level = level;
    }

    public SkillDto(String name) {
        this.name = name;
    }

    public SkillDto() {
    }

    /*@Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SkillDto skillDto = (SkillDto) o;

        return new EqualsBuilder().append(name, skillDto.name).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(name).toHashCode();
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SkillDto skillDto = (SkillDto) o;

        return new EqualsBuilder().append(name, skillDto.name).append(level, skillDto.level).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(name).append(level).toHashCode();
    }
}
