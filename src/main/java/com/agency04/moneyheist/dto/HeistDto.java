package com.agency04.moneyheist.dto;

import com.agency04.moneyheist.entity.HeistStatus;

import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

public class HeistDto {

    @NotBlank
    private String name;

    @NotBlank
    private String location;

    @Future
    private LocalDateTime startTime;

    @Future
    private LocalDateTime endTime;

    private HeistStatus status = HeistStatus.PLANNING;

    @Valid
    private List<HeistSkillDto> skills;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public List<HeistSkillDto> getSkills() {
        return skills;
    }

    public void setSkills(List<HeistSkillDto> skills) {
        this.skills = skills;
    }

    public HeistStatus getStatus() {
        return status;
    }

    public void setStatus(HeistStatus status) {
        this.status = status;
    }
}
