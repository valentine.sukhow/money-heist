package com.agency04.moneyheist.dto;

import java.util.Set;

public class SkillsDto {

    private Set<SkillDto> skills;

    private String mainSkill;

    public Set<SkillDto> getSkills() {
        return skills;
    }

    public void setSkills(Set<SkillDto> skills) {
        this.skills = skills;
    }

    public String getMainSkill() {
        return mainSkill;
    }

    public void setMainSkill(String mainSkill) {
        this.mainSkill = mainSkill;
    }
}
