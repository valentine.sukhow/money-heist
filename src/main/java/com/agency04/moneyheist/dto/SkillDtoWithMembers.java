package com.agency04.moneyheist.dto;

public class SkillDtoWithMembers extends SkillDto {

    private Integer members;

    public Integer getMembers() {
        return members;
    }

    public void setMembers(Integer members) {
        this.members = members;
    }

    public SkillDtoWithMembers(String name, String level, Integer members) {
        super(name, level);
        this.members = members;
    }

}
