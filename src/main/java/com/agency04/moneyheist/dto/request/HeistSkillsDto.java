package com.agency04.moneyheist.dto.request;

import com.agency04.moneyheist.dto.HeistSkillDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class HeistSkillsDto {

    @NotNull
    @Valid
    private List<HeistSkillDto> skills;

    public List<HeistSkillDto> getSkills() {
        return skills;
    }

    public void setSkills(List<HeistSkillDto> skills) {
        this.skills = skills;
    }
}
