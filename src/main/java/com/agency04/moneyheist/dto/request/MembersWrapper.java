package com.agency04.moneyheist.dto.request;

import javax.validation.constraints.NotNull;
import java.util.List;

public class MembersWrapper {

    @NotNull
    public List<String> members;

}
