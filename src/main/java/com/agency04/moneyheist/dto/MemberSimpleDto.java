package com.agency04.moneyheist.dto;

import java.util.Set;

public class MemberSimpleDto {

    public String name;

    public Set<SkillDto> skills;

    public MemberSimpleDto(String name, Set<SkillDto> skills) {
        this.name = name;
        this.skills = skills;
    }
}
